class TestAddFormOwner:

    @tc_id('FPS_094')
    def test_rent_long_geo_house_1_room_flat(self): #todo Проверка на упрощенную форму подачи после инфы от Оли
        params = {
            'address': 'Россия, Санкт-Петербург, Университетская набережная, 13Е'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_flat_long_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_FLAT_FPS_094)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_FLAT_FPS_094)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_bundle()

        check_elastic_search(add_form_page.publish_and_get_id(), about_object_data.RENT_FLAT_FPS_094,
                             bargain_terms_data.RENT_FLAT_FPS_094)

    @tc_id('FPS_095')
    def test_rent_long_geo_house_2_room_flat(self):
        params = {
            'address': 'Россия, Москва, Молодогвардейская улица, 29к2',
            'subways': ['Молодежная', 'Кунцевская']
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_flat_long_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.Address.NearestSubway().check_subway_stations(params['subways'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_FLAT_FPS_095)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_FLAT_FPS_095)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_bundle()
        check_elastic_search(add_form_page.publish_and_get_id(), about_object_data.RENT_FLAT_FPS_095,
                             bargain_terms_data.RENT_FLAT_FPS_095)

    @tc_id('FPS_096')
    def test_rent_long_geo_house_3_room_flat(self):
        params = {
            'address': 'Россия, Москва, Сиреневый бульвар, 22',
            'subways': ['Щелковская', 'Первомайская', 'Измайловская']
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_flat_long_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.Address.NearestSubway().block.wait_for_visible()
        add_form_page.Address.NearestSubway().check_subway_stations(params['subways'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_FLAT_FPS_096)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_FLAT_FPS_096)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_paid()
        check_elastic_search(add_form_page.publish_and_get_id(), about_object_data.RENT_FLAT_FPS_096,
                             bargain_terms_data.RENT_FLAT_FPS_096)

    @tc_id('FPS_097')
    def test_rent_long_geo_house_bed(self):
        params = {
            'address': 'Россия, Москва, Ялтинская улица, 5'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_bed_rent_long_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_ROOM_FPS_097)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_ROOM_FPS_097)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_paid()
        check_elastic_search(add_form_page.publish_and_get_id(), about_object_data.RENT_ROOM_FPS_097,
                             bargain_terms_data.RENT_ROOM_FPS_097)

    @tc_id('FPS_098')
    def test_rent_long_geo_house_room(self):
        params = {
            'address': 'Россия, Москва, Амурская улица, 5с19',
            'subways': ['Черкизовская', 'Локомотив', 'Бульвар Рокоссовского']
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_room_long_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.Address.NearestSubway().check_subway_stations(params['subways'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_ROOM_FPS_098)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_ROOM_FPS_098)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_free()
        check_elastic_search(add_form_page.publish_and_get_id(), about_object_data.RENT_ROOM_FPS_098,
                             bargain_terms_data.RENT_ROOM_FPS_098)

    @tc_id('FPS_099')
    def test_rent_long_geo_district_dacha(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_suburban_long_owner()
        add_form_page.Address.fill_address(address_data.RENT_DACHA_FPS_099['address'])
        add_form_page.Address.Highway().add_highway(address_data.RENT_DACHA_FPS_099['highway'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_DACHA_FPS_099)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_DACHA_FPS_099)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_bundle()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             address_data.RENT_DACHA_FPS_099,
                             about_object_data.RENT_DACHA_FPS_099,
                             bargain_terms_data.RENT_DACHA_FPS_099)

    @tc_id('FPS_100')
    def test_rent_long_geo_district_cottage(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_suburban_cottage_long_owner()
        add_form_page.Address.fill_address(address_data.RENT_COTTAGE_FPS_100['address'])
        add_form_page.Address.Highway().add_highway(address_data.RENT_COTTAGE_FPS_100['highway'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_COTTAGE_FPS_100)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_COTTAGE_FPS_100)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_premium()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             address_data.RENT_COTTAGE_FPS_100,
                             about_object_data.RENT_COTTAGE_FPS_100,
                             bargain_terms_data.RENT_COTTAGE_FPS_100)

    @tc_id('FPS_101')
    def test_rent_long_geo_district_townhouse(self):
        params = {
            'address': 'Россия, Московская область, Раменское, Хрипанский проезд'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_townhouse_long_owner()
        add_form_page.Address.fill_address_without_geo_suggest(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_TOWNHOUSE_FPS_101)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_TOWNHOUSE_FPS_101)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_bundle()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_TOWNHOUSE_FPS_101,
                             bargain_terms_data.RENT_TOWNHOUSE_FPS_101)

    @tc_id('FPS_102')
    def test_rent_long_geo_district_part_house(self):
        params = {
            'address': 'Россия, Московская область, Ленинский район, поселок городского типа Горки Ленинские, Административная улица'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_suburban_share_long_owner()
        add_form_page.Address.fill_address_without_geo_suggest(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_PART_HOUSE_FPS_102)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_PART_HOUSE_FPS_102)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_free()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_PART_HOUSE_FPS_102,
                             bargain_terms_data.RENT_PART_HOUSE_FPS_102)

    @tc_id('FPS_103')
    def test_rent_daily_flat_owner(self):
        params = {
            'address': 'Россия, Москва, Амурская улица, 5с19'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_flat_daily_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_FLAT_FPS_103)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_FLAT_FPS_103)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_paid()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_FLAT_FPS_103,
                             bargain_terms_data.RENT_FLAT_FPS_103)

    @tc_id('FPS_104')
    def test_rent_daily_room_owner(self):
        params = {
            'address': 'Россия, Москва, Амурская улица, 5с19'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_room_short_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_ROOM_FPS_104)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_ROOM_FPS_104)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_premium()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_ROOM_FPS_104,
                             bargain_terms_data.RENT_ROOM_FPS_104)

    @tc_id('FPS_105')
    def test_rent_daily_house_owner(self):
        params = {
            'address': 'Россия, Москва, Амурская улица, 5с19'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_house_few_mounth_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_HOUSE_FPS_105)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_HOUSE_FPS_105)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_free()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_HOUSE_FPS_105,
                             bargain_terms_data.RENT_HOUSE_FPS_105)

    @tc_id('FPS_106')
    def test_rent_few_mounth_one_room_flat_owner(self):
        params = {
            'address': 'Россия, Москва, Малая Филёвская улица, 18'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_flat_few_mounth_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_FLAT_FPS_106)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_FLAT_FPS_106)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_bundle()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_FLAT_FPS_106,
                             bargain_terms_data.RENT_FLAT_FPS_106)

    @tc_id('FPS_107')
    def test_rent_few_mounth_two_room_flat_owner(self):
        params = {
            'address': 'Россия, Москва, Молодогвардейская улица, 29к2',
            'subways': ['Молодежная', 'Кунцевская']
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_flat_few_mounth_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_FLAT_FPS_107)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_FLAT_FPS_107)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_paid()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_FLAT_FPS_107,
                             bargain_terms_data.RENT_FLAT_FPS_107)

    @tc_id('FPS_108')
    def test_rent_few_mounth_three_room_flat_owner(self):
        params = {
            'address': 'Россия, Москва, Сиреневый бульвар, 22',
            'subways': ['Щелковская', 'Первомайская', 'Измайловская']
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_flat_few_mounth_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.Address.NearestSubway().block.wait_for_visible()
        add_form_page.Address.NearestSubway().check_subway_stations(params['subways'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_FLAT_FPS_108)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_FLAT_FPS_108)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_paid()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_FLAT_FPS_108,
                             bargain_terms_data.RENT_FLAT_FPS_108)

    @tc_id('FPS_109')
    def test_rent_few_mounth_room_owner(self):
        params = {
            'address': 'Россия, Москва, Ялтинская улица, 5',
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_room_few_mounth_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_ROOM_FPS_109)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_ROOM_FPS_109)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_bundle()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_ROOM_FPS_109,
                             bargain_terms_data.RENT_ROOM_FPS_109)

    @tc_id('FPS_110')
    def test_rent_few_mounth_bed_owner(self):
        params = {
            'address': 'Россия, Москва, Амурская улица, 5с19',
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_bed_few_mounth_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_BED_FPS_110)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_BED_FPS_110)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_free()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_BED_FPS_110,
                             bargain_terms_data.RENT_BED_FPS_110)

    @tc_id('FPS_111')
    def test_rent_few_mounth_house_owner(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_house_few_mounth_owner()
        add_form_page.Address.fill_address(address_data.RENT_HOUSE_FPS_111['address'])
        add_form_page.Address.Highway().add_highway(address_data.RENT_HOUSE_FPS_111['highway'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_HOUSE_FPS_111)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_HOUSE_FPS_111)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_bundle()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             address_data.RENT_HOUSE_FPS_111,
                             about_object_data.RENT_HOUSE_FPS_111,
                             bargain_terms_data.RENT_HOUSE_FPS_111)

    @tc_id('FPS_112')
    def test_rent_few_mounth_cottage_owner(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_suburban_cottage_few_mounth_owner()
        add_form_page.Address.fill_address(address_data.RENT_COTTAGE_FPS_112['address'])
        add_form_page.Address.Highway().add_highway(address_data.RENT_COTTAGE_FPS_112['highway'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_COTTAGE_FPS_112)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_COTTAGE_FPS_112)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_premium()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             address_data.RENT_COTTAGE_FPS_112,
                             about_object_data.RENT_COTTAGE_FPS_112,
                             bargain_terms_data.RENT_COTTAGE_FPS_112)

    @tc_id('FPS_113')
    def test_rent_few_mounth_townhouse_owner(self):
        params = {
            'address': 'Россия, Москва, Амурская улица, 5с19',
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_townhouse_few_mounth_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_TOWNHOUSE_FPS_113)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_TOWNHOUSE_FPS_113)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_premium()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_TOWNHOUSE_FPS_113,
                             bargain_terms_data.RENT_TOWNHOUSE_FPS_113)

    @tc_id('FPS_114')
    def test_rent_few_mounth_part_house_owner(self):
        params = {
            'address': 'Россия, Московская область, Балашиха, микрорайон Саввино, Пригородная улица, 99',
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_rent_house_share_few_mounth_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.RENT_PART_HOUSE_FPS_114)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.RENT_PART_HOUSE_FPS_114)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_premium()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.RENT_PART_HOUSE_FPS_114,
                             bargain_terms_data.RENT_PART_HOUSE_FPS_114)

    SALE_LIVING_ADDRESS = 'Россия, Москва, Малая Филёвская улица, 18'

    @tc_id('FPS_115')
    def test_sale_flat_one_room_owner(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_sale_flat_owner()
        add_form_page.Address.fill_address(self.SALE_LIVING_ADDRESS)
        add_form_page.AboutObject.fill(**about_object_data.SALE_FLAT_FPS_115)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.SALE_FLAT_FPS_115)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_premium()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.SALE_FLAT_FPS_115,
                             bargain_terms_data.SALE_FLAT_FPS_115)

    @tc_id('FPS_116')
    def test_sale_flat_two_room_owner(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_sale_flat_owner()
        add_form_page.Address.fill_address(self.SALE_LIVING_ADDRESS)
        add_form_page.AboutObject.fill(**about_object_data.SALE_FLAT_FPS_116)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.SALE_FLAT_FPS_116)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_top_3()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.SALE_FLAT_FPS_116,
                             bargain_terms_data.SALE_FLAT_FPS_116)

    @tc_id('FPS_117')
    def test_sale_flat_three_room_owner(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_sale_flat_owner()
        add_form_page.Address.fill_address(self.SALE_LIVING_ADDRESS)
        add_form_page.AboutObject.fill(**about_object_data.SALE_FLAT_FPS_117)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.SALE_FLAT_FPS_117)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_top_3()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.SALE_FLAT_FPS_117,
                             bargain_terms_data.SALE_FLAT_FPS_117)

    @tc_id('FPS_118')
    def test_sale_room_owner(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_sale_room_owner()
        add_form_page.Address.fill_address(self.SALE_LIVING_ADDRESS)
        add_form_page.AboutObject.fill(**about_object_data.SALE_ROOM_FPS_118)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.SALE_ROOM_FPS_118)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_top_3()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.SALE_ROOM_FPS_118,
                             bargain_terms_data.SALE_ROOM_FPS_118)

    @tc_id('FPS_119')
    def test_sale_flat_share_owner(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_sale_flat_share_owner()
        add_form_page.Address.fill_address(self.SALE_LIVING_ADDRESS)
        add_form_page.AboutObject.fill(**about_object_data.SALE_FLAT_SHARE_FPS_119)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.SALE_FLAT_SHARE_FPS_119)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_top_3()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.SALE_FLAT_SHARE_FPS_119,
                             bargain_terms_data.SALE_FLAT_SHARE_FPS_119)

    @tc_id('FPS_120')
    def test_sale_house_owner(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_sale_house_owner()
        add_form_page.Address.fill_address(address_data.SALE_HOUSE_FPS_120['address'])
        add_form_page.Address.Highway().add_highway(address_data.SALE_HOUSE_FPS_120['highway'])
        add_form_page.AboutObject.fill(**about_object_data.SALE_HOUSE_FPS_120)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.SALE_HOUSE_FPS_120)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_top_3()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.SALE_HOUSE_FPS_120,
                             bargain_terms_data.SALE_HOUSE_FPS_120)

    @tc_id('FPS_121')
    def test_sale_cottage_owner(self):
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_sale_cottage_owner()
        add_form_page.Address.fill_address(address_data.SALE_COTTAGE_FPS_121['address'])
        add_form_page.Address.Highway().add_highway(address_data.SALE_COTTAGE_FPS_121['highway'])
        add_form_page.AboutObject.fill(**about_object_data.SALE_COTTAGE_FPS_121)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.SALE_COTTAGE_FPS_121)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_top_3()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.SALE_COTTAGE_FPS_121,
                             bargain_terms_data.SALE_COTTAGE_FPS_121)

    @tc_id('FPS_122')
    def test_sale_townhouse_owner(self):
        params = {
            'address': 'Россия, Московская область, Раменское, улица Михалевича, 4'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_sale_townhouse_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.SALE_TOWNHOUSE_FPS_122)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.SALE_TOWNHOUSE_FPS_122)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_paid()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.SALE_TOWNHOUSE_FPS_122,
                             bargain_terms_data.SALE_TOWNHOUSE_FPS_122)

    @tc_id('FPS_123')
    def test_sale_house_share_owner(self):
        params = {
            'address': 'Россия, Санкт-Петербург, набережная реки Мойки, 48-50-52'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_sale_house_share_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.SALE_HOUSE_SHARE_FPS_123)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.SALE_HOUSE_SHARE_FPS_123)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_premium()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.SALE_HOUSE_SHARE_FPS_123,
                             bargain_terms_data.SALE_HOUSE_SHARE_FPS_123)

    @tc_id('FPS_124')
    def test_sale_house_share_owner(self):
        params = {
            'address': 'Россия, Санкт-Петербург, набережная реки Мойки, 48-50-52'
        }
        AuthPage().authorise(qa_api.get_owner_user())
        add_form_page = AddFormPage()
        add_form_page.apply_sale_land_owner()
        add_form_page.Address.fill_address(params['address'])
        add_form_page.AboutObject.fill(**about_object_data.SALE_LAND_FPS_124)
        add_form_page.Description.upload_test_images(5)
        add_form_page.Description.fill_description()
        add_form_page.BargainTerms.fill(**bargain_terms_data.SALE_LAND_FPS_124)
        add_form_page.ContactInformation.select_first_number()
        add_form_page.PublishTerms.select_premium()
        check_elastic_search(add_form_page.publish_and_get_id(),
                             about_object_data.SALE_LAND_FPS_124,
                             bargain_terms_data.SALE_LAND_FPS_124)