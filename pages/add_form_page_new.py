class AddFormPage(Page):
    PAGE_PATH = 'realty/add1.aspx'

    Header = Header()

    _form_title = Locator(By.XPATH, '//div[@class="add-form__title"]', 'Заголовок формы')
    account_type = Switcher(By.XPATH, '//div[@ng-model="adTypeCtrl.ad.isByHomeowner"]', 'переключатель "Тип аккаунта"')
    active_form_switcher = Locator(By.XPATH, "//*[@class='short-form-switcher']//*[contains(@class,'active')]", 'Активный тип формы')
    short_form = Locator(By.CSS_SELECTOR, ".short-form-switcher__short-form", 'Переключатель на короткую форму подачи')
    full_form = Locator(By.CSS_SELECTOR, ".short-form-switcher__full-form", 'Переключатель на полную форму подачи')
    publish_button = Button(By.XPATH, '//button[@class="cui-button cui-button_look_solid"]', 'Разместить обьявление')
    premoderation_error = Div(By.XPATH, '//div[contains(@ng-if, "PremoderationError")]', 'Ошибка премодерации "В объявлении найдены нарушения ..."')
    publish_with_errors_btn = Locator(By.XPATH, '//a[contains(@ng-click, "SkipPremoderationError")]', 'Кнопка "Разместить с нарушениями"')
    add_form_preloader = Div(By.XPATH, '//div[@class="add-form__preloader"]', 'Прелоадер')
    add_form_error_refill = Div(By.XPATH, '//div[@class="modal-refill"]', 'Попап ошибки подачи обьявления')
    add_form_error_refill_close = Div(By.XPATH, '//div[@class="modal-refill"]//..//..//button[@class="cui-modal__close"]', 'Кнопка закрытия попапа с ошибкой')
    short_form_switcher = Div(By.XPATH, '//div[@class="short-form-switcher"]', 'Переключатель формы подачи')
    uploaded_photos = Div(By.XPATH, '//div[@class="cui-photo-upload__photos"]//div[@class="cui-photo-upload__photo-item"]', 'Загруженные фотографии')
    _selected_object_type = None


    class BaseClosePopupBeforePublish(Block):
        allure_prefix = 'Базовое описание попапа, который нужно закрыть перед публикацией объявки'

        block = OverrideMe(By.XPATH, None, 'Контэйнер попапа')
        cancel = OverrideMe(By.XPATH, None, 'Кнопка "Отменить"/"Закрыть"')
        continue_btn = OverrideMe(By.XPATH, None, 'Кнопка "Продолжить"')

    class ChangeActivePlacement(Block):
        allure_prefix = 'Попап "Изменение активного размещения"'
        block = Locator(By.XPATH, "//div[@class='check-publish']", 'Блок')
        cancel = Locator(By.XPATH, "//div[@class='check-publish']//button[.='Отменить']", 'Кнопка "Отменить"')
        continue_btn = Locator(By.XPATH, "//div[@class='check-publish']//button[.='Продолжить']", 'Кнопка "Продолжить"')

    class SanctionPopup(Block):
        allure_prefix = 'Модальное окно ошибки публикации'
        block = Locator(By.XPATH, '//div[@class="cui-modal__dialog"]', 'Контейнер блока элементов')

        amend_btn = Button(By.XPATH, '//button[.="Исправить"]', 'Кнопка "Исправить"')
        publish_with_errors_btn = Button(By.XPATH, '//button[.="Разместить с нарушениями"]', 'Кнопка "Разместить с нарушениями"')

    class AuctionPopupBase(Block):
        block = Locator(By.XPATH, "//div[@class='cui-modal__dialog'][.//div[contains(@class,'auction')]]", 'Блок')
        close = Locator(By.XPATH, "//div[@class='cui-modal__dialog'][.//div[contains(@class,'auction')]]//*[contains(@data-mark,'close')]", 'Кнопка закрыть')

    class MotivationAuctionPopup(AuctionPopupBase):
        allure_prefix = 'Мотивационный попап "Добавьте ставку аукциона"'
        add_rate = Locator(By.XPATH, "//div[contains(@class,'auction')]//button[.='Добавить ставку']", 'Добавить ставку')
        continue_without_rate = Locator(By.XPATH, "//div[contains(@class,'auction')]//button[.='Продолжить без ставки']", 'Продолжить без ставки')

    class CancelAuctionRatePopup(AuctionPopupBase):
        allure_prefix = 'Попап "Вы отменяете ставку аукциона"'
        continue_btn = Locator(By.XPATH, "//div[contains(@class,'auction')]//span[text()='Продолжить']", 'Кнопка "Продолжить"')

    class IncreaseRatePopup(AuctionPopupBase):
        allure_prefix = 'Попап "Повысьте ставку аукциона"'
        title = Locator(By.XPATH, "//div[contains(@class, 'modal_for_auction_increase')]//div[contains(@class, '__title')]", 'Заголовок попапа')
        new_rate = Input(By.XPATH, "//div[contains(@class, 'modal_for_auction_increase')]//input[@name='auctionBet']", "Инпут новая ставка")
        apply = Locator(By.XPATH, "//div[contains(@class, 'modal_for_auction_increase')]//button[.='Применить']", "Кнопка Применить")
        continue_with_current_rate = Locator(By.XPATH, "//div[contains(@class, 'modal_for_auction_increase')]//a[contains(text(), 'с действующей ставкой')]", "Продолжить с действующей ставкой")

    save_draft_btn = Locator(By.XPATH, "//button[@button-type='submit'][@ng-mousedown='adSubmitButtons.draftAd()']", 'Кнопка сохранения в черновик')

    active_draft_checkbox_hidden_input_l = Locator(By.XPATH, "//input[@ng-model='activeDraft.getterSetter']", 'Чекбокс Активный черновик (скрытый input)')
    active_draft_checkbox_clickable_l = Locator(By.XPATH, '//label[.//input[@ng-model="activeDraft.getterSetter"]]', 'Чекбокс Активный черновик')
    active_draft_checkbox = Div(By.XPATH, '//div[./label[.//input[@ng-model="activeDraft.getterSetter"]]]', 'Состояние чекбокса')
    active_draft_enable_button = Locator(By.XPATH, "//span[text()='Включить']/..", 'Кнопка подтверждения включения активного черновика')
    active_draft_disable_button_l = Locator(By.XPATH, "//span[text()='Выключить']/..", 'Кнопка подтверждения включения активного черновика')

    validation_error_tooltip = Locator(By.XPATH, '//div[@class="cui-tooltip__inner"]', 'Подсказка ошибки валидации значения')
    photo_count_warning = Div(By.XPATH, '//div[@class="cui-tooltip right right-top in"]', 'Ошибка публикации фотографий')

    # Auth options
    registration_tab = Locator(By.XPATH, '//li[@heading="Регистрация"]', 'Вкладка Регистрации')

    # Offer type
    deal_type = Switcher(By.XPATH, '//div[contains(@data-mark,"switcher|adType.dealType")]', 'Тип сделки')
    property_type = Switcher(By.XPATH, '//div[contains(@data-mark,"switcher|adType.propertyType")]', 'Тип недвижимости')

    disclosed_publish_type_block = Locator(By.XPATH, '//div[contains(@class,"type-segment__bar_expanded")]', 'Свернувшийся блок "Тип объявления"')
    edit_publish_type_btn = Button(By.XPATH, '//div[contains(@class,"type-segment__bar_expanded")]//span[./button]', 'Изменить тип объекта')
    object_radio_buttons = ObjectType(By.XPATH, '//div[@class="field field_type_object"]', 'Объект')

    refill_modal = Locator(By.XPATH, '//div[@class="modal-refill"]', 'Ошибка, внесите изменения')

    # Блоки на ФП, которые появляются после авторизации
    publish_terms_block = Locator(By.ID, 'ad-placing-terms', 'Блок "Условия размещения"')

    # Price
    auction_info = Locator(By.XPATH, "//div[contains(@class,'cui-placing-terms__total') and contains(text(),'аукцион')]", "Информация про ставку аукциона")
    calltracking_price = Locator(By.XPATH, "//div[contains(@class,'cui-placing-terms__total_option') and contains(text(), 'Защита номера')]", 'Цена за колтрекинг')
    publishing_price = Locator(By.XPATH, "//div[@class='cui-placing-terms__total-price']", 'Общая цена')

    flocktory_footer = Locator(By.XPATH, "//iframe[@class='flocktory-widget' and @id]", 'флоктори футер "Используйте платные типы размещения"')
    close_flock_footer = Locator(By.XPATH, "//div[@class='Ribbon-close']", 'закрыть флоктори-футер')

    class RefillModal(Component):
        modal_title = Locator(By.XPATH, './/div[@class="af-modal__title"]', 'Заголовок')
        modal_body = Locator(By.XPATH, './/div[@class="af-modal__body"]', 'Тело')
        modal_close = Locator(By.XPATH, './/button[@class="cui-modal__close"]', 'Закрыть')

    class Authorisation:
        registration_form_parent = Locator(By.XPATH, '//div[./ng-form[@name="regSimpleCtrl.form"]]', 'Форма регистрации')
        im_professional_checkbox = Locator(By.XPATH, '//label[.//input[@name="isProfessional"]]', 'Я профессионал')
        subscription_checkbox = Locator(By.XPATH, '//label[.//input[@name="subscription"]]', 'Хочу получать новости Cian.ru')
        register_button = Button(By.XPATH, '//div[@class="auth-form__action"]//button[@cian-ui-ng-form-submit-button]', 'Зарегистрироваться')
        name_field = Input(By.XPATH, '//input[@name="name"]', 'Имя')
        phone_number = Input(By.XPATH, '//input[@name="number"][not(@tabindex)]', 'Телефон')
        confirm_phone_button = Button(By.XPATH, '//div[@class="auth-form__phone-button"]//button', 'Подтвердить телефон')
        confirm_phone_field = Input(By.XPATH, '//input[@data-mark="input|phoneConfirmation.code"]', 'Поле ввода кода подтверждения')
        email_field = Input(By.XPATH, '//input[@name="email"]', 'ID или Email')
        login_password_field = Input(By.XPATH, '//input[@name="loginPassword"]', 'Пароль')
        registration_password_field = Input(By.XPATH, '//input[@name="registrationPassword"]', 'Пароль')
        remember_me_checkbox = Locator(By.XPATH, '//label[.//span[text()="Запомнить меня"]]', 'Запомнить меня')
        enter_button = Button(By.XPATH, '//button[.//span[text()="Войти"]]', 'Войти')

        fb_register_button = Div(By.XPATH, '//div[contains(@class,"social-login__button_fb")]', 'Кнопка Регистрации через ФБ')
        vk_register_button = Div(By.XPATH, '//div[contains(@class,"social-login__button_vk")]', 'Кнопка Регистрации через VK')

        @allure_step('Логинем пользователя {} на форме подаче')
        def login_user(self, user):
            self.email_field().send_keys(user.email)
            self.login_password_field().wait_for_visible().send_keys(user.password)
            self.enter_button().wait_clickable().click()

    class SanctionModal:
        modal_title = Div(By.XPATH, "//div[@class='af-modal__title']", 'Заголовок модального окна')
        modal_body = Div(By.XPATH, "//div[@class='af-modal__body']", 'Тело модального окна')

    class SelectRatePopup(Block):
        allure_prefix = 'попап "Выберите тариф или пройдите идентификацию аккаунта..."'

        title_str = 'Выберите тариф или пройдите идентификацию аккаунта, чтобы опубликовать объявление'

        container = Locator(By.XPATH, "//div[@class='cui-modal__dialog']", 'контейнер')
        title = Locator(By.XPATH, "//div[@class='cui-modal__dialog']//div[@class='af-modal__title']", 'Заголовок')
        continue_btn = Button(By.XPATH, '//button[./span[.="Продолжить"]]', 'Кнопка "Продолжить"')
        close_btn = Locator(By.XPATH, "//button[@data-mark='button|modal|close']", 'кнопка Закрыть')

        @allure_step('Проверяем и закрываем попап "Выберите тариф или пройдите идентификацию аккаунта"')
        def check_and_close(self):
            assert_displayed([
                self.container,
                self.title,
                self.continue_btn
            ])
            self.title().assert_text(equals=self.title_str, stripped=True)
            self.close_btn().click()
            self.container.wait_for_hide()

    class VisibilityOption(Enum):
        visible_for_all = 'Видно всем'
        visible_for_agents = 'Видно только агентам'

    visibility = Switcher(By.XPATH, '//div[contains(@data-mark,"switcher|ad.isInHiddenBase")]', 'Выбор видимости объекта')

    class Rosreestr(Block):
        allure_prefix = 'Блок "Получите дополнительные преимущества для своего объявления бесплатно"'
        block = Locator(By.XPATH, '//cian-ui-compile-directive[@id="homeowner"]', 'Контейнер блока элементов')

        acquire_btn = Locator(By.XPATH, '//cian-ui-compile-directive[@id="homeowner"]//button[.="Получить"]', 'Кнопка "Получить"')
        cadastral_number_link = Locator(By.XPATH, '//cian-ui-compile-directive[@id="homeowner"]//a', 'Ссылка "Где найти кадастровый номер?"')
        cadastral_number_hint_block = Locator(By.XPATH, '//div[@popup-class]', 'Попап подсказки кадастрового номера')
        close_btn = Locator(By.XPATH, '//div[@class="homeowner__fields"]//*[@name="close"]', 'Кнопка закрытия полей ввода')